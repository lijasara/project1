import time
from celery import Celery
from celery import chord
from celery.schedules import crontab
from celery.task import periodic_task

app = Celery('helloworld', backend='redis://', broker='amqp://guest@//')

app.config_from_object(__name__)


@app.task()
def main_task():
    task1_list = [first_print.s() for x in range(5)]
    task2_list = [second_print.s() for x in range(10)]
    task3_list = [third_print.s()]
    task_group = task1_list + task2_list + task3_list
    chord(task_group, call_back.s())()

    #sub_task1.delay()
    # res1 = group(first_print.si() for i in xrange(5))()
    # res2 = group(second_print.si() for i in xrange(10))()
    # res3 = group(third_print.si() for i in xrange(15))()
    # res4 = res1.get()+res2.get()+res3.get()
    # res = str(res4)
    # f = open("link.txt", "w")
    # f.write(res)
    #res3 = group(third_print.si() for i in xrange(15))().get()
    # job = group([
    #     first_print.si() for i in range(5),
    #     second_print.si() for i in range(5),
    #     third_print.si() for i in range(5)
    # ])
    # res = result = job.apply_async().get()
    #chord((res4), call_back.s()).delay()
    #chord((res), call_back.s()).apply_async()
#    chord((first_print.s() for i in range(5)), (call_back.s())).delay()
    return "main_task"


@app.task()
def first_print():

    return "first_print"


@app.task()
def second_print():
    return "second_print"


@app.task()
def third_print():
    time.sleep(30)
    return "third_print"


@app.task()
def call_back(*args, **kwargs):
    return "call_back"

#
# @app.task()
# def sub_task():
#     first_group = [first_print.s() for x in range(5)]
#     second_group = [second_print.s() for x in range(10)]
#     third_group = [third_print.s() for x in range(15)]
#     task_group = first_group + second_group +third_group
#     chord(task_group, call_back.s())()
#


# @app.task()
# def sub_task1():
#     res1 = group(first_print.si() for i in xrange(5))()
#     res2 = group(second_print.si() for i in xrange(10))()
#     res3 = res1.get +res2.get()
#     chord(res3, call_back.s())()
#

@periodic_task(run_every=crontab())
def hello():
    return "hello"




