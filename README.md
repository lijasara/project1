# Swapi Data Connector
Swapi Data Connector is a Python package to connect to football-data.org API

##Installation

###Requirements
Swapi data connector requires Python 3.4 or later. This package is not tested with any version of Python 2.7. The following third party packages are required, which will be auto-installed if you are using pip

*python-dateutil
*requests

###Installing via pip

The easiest way to install swapi data connector is using the Python package manager, pip. Use the following command if you have pip installed

pip install swapi-data-connector

###Installing from github

If you need the latest version, you can install it directly from github repository.

pip3 install git+https://lijasara@bitbucket.org/lijasara/project1.git

##User Guide

>Creating a Connection to swapi-data.org API

Before retrieving data, you need to create a connection object.

from swapidata.connector import Connector
```
connection = Connector()
The Connector constructor.
```

from swapidata.connector import Connector

connection = Connector()

### Connector object attributes

* base_url - Gives the base API URL
* competitions_endpoint - Gives the API endpoint to fetch competitions

##Connector object methods

####get_peoples()

Returns a DataSet object contains People objects.

####get_vehicles()

Returns a DataSet object contains People objects.

####get_species()

Returns a DataSet object contains People objects.

####get_startships()

Returns a DataSet object contains People objects.

####get_planets()

Returns a DataSet object contains People objects.

###DataSet Objects

Methods like get_peoples, get_vehicles etc will return a DataSet object. DataSet is an iterable object. You can use it like any other iterable such as list, tuple etc. Operations like using with a for loop, checking length using len, subscripting, slicing, reversing etc are supported

### DataSet Objects

Methods like *get_peoples*, *get_vehicles* etc will return a **DataSet** object. **DataSet** is an iterable object.
You can use it like any other iterable such as **list**, **tuple** etc. Operations like using with a *for* loop,
checking length using *len*, subscripting, slicing, reversing etc are supported


### People Objects

A **People** object represents a people in swapi-data.org API

### Vehicle Objects

A **Vehicle** object represents a vehicle in swapi-data.org API

### Species Objects

A **Species** object represents a species in swapi-data.org API

### Starship Objects

A **Starship** object represents a starship in swapi-data.org API

### Planets Objects

A **Planet** object represents a planet in swapi-data.org API

### Films Objects

A **Film** object represents a film in swapi-data.org API


##Caching and Lazy Evaluation

Values in a **DataSet** object are cached during the creation of **DataSet**. Subsequent calls to methods which returns
a **DataSet** will be returning the cached values.

A **DataSet** will not perform any API calls during its creation. There will not be any values in a **DataSet** after
its creation. API call is executed only when an action which uses the data is executed such as using in a for loop,
checking the length of **DataSet** etc.



