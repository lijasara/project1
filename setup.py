from setuptools import setup


setup(
    name='Swapi Data Connector',
    version='0.1.0',
    url='https://lijasara@bitbucket.org/lijasara/project1.git',
    license='BSD',
    author='Lija Sara Eapen',
    author_email='lija@labglo.com',
    description='Python package to connect to swapi-data.org API',
    packages=['swapidata'],
    include_package_data=True,
    install_requires=[
        'python-dateutil>=2.6.1',
        'requests>=2.18.3',
    ],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Software Development :: Libraries',
    ]
)