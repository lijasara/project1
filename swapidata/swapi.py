class Swapi:
    """user defined class"""

    def __init__(self, **kwargs):
        for key in kwargs:
            setattr(self, key, kwargs[key])

    def __repr__(self):
        return self.name


class Films(Swapi):
    """user defined films class"""

    pass


class Planets(Swapi):
    """user defined planets class"""

    pass


class People(Swapi):
    """user defined people class"""

    pass


class Species(Swapi):
    """user defined vehicles class"""

    pass


class Vehicles(Swapi):
    """user defined species class"""

    pass


class StarShips(Swapi):
    """user defined species class"""

    pass

