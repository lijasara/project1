import requests


def collect_data_from_api(endpoint):
    """Fetches data from the endpoint and returns it
    :param endpoint: api endpoint to connect
    :return:
    """

    r = requests.get(endpoint)

    # Return the json data if request is successful
    if r.status_code == 200:
        current_data = r.json()
        required_data = r.json()['results']
        while (current_data['next']):
            current_data = requests.get(current_data['next']).json()
            required_data.extend(current_data['results'])
        return required_data

    return []

