from .datasets import DataSet
from .swapi import People, Species, Films, Vehicles, StarShips, Planets
from .utils import collect_data_from_api


class Connector:
    """Class to initialise connection to swapi-data.org"""

    def __init__(self):
        """Initialises connection to swapi-data.org

        :return: Connector object
        """
        self.base_url = "https://swapi.co/api/"
        self.__peoples = []
        self.__starships = []
        self.__vehicles = []
        self.__species = []
        self.__planets = []
        self.__films = []

    def get_peoples(self):

        self.__peoples = DataSet(klass=People, endpoint="{base_url}people/".format(base_url=self.base_url))
        return self.__peoples

    def get_startships(self):

        self.__starships = DataSet(klass=StarShips, endpoint="{base_url}starships/".format(base_url=self.base_url))
        return self.__starships

    def get_vehicles(self):

        self.__vehicles = DataSet(klass=Vehicles, endpoint="{base_url}vehicles/".format(base_url=self.base_url))
        return self.__vehicles

    def get_species(self):

        self.__species = DataSet(klass=Species, endpoint="{base_url}vehicles/".format(base_url=self.base_url))
        return self.__species

    def get_planets(self):

        self.__planets = DataSet(klass=Planets, endpoint="{base_url}planets/".format(base_url=self.base_url))
        return self.__planets

    def get_films(self):

        self.__films = DataSet(klass=Films, endpoint="{base_url}films/".format(base_url=self.base_url))
        return self.__films

