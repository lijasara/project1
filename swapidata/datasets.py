from .utils import collect_data_from_api


class DataSet:
    """Class to represent a sequence of swapi data objects"""

    def __init__(self, klass, endpoint=''):
        """Initialises SwapiDataObject sequence
               :param klass: type, Class of objects in data set. Should be either FootballDataObject or its subclass
               :param endpoint: str, API endpoint to fetch data
               :param data_list: iterable containing klass objects
               """
        self._klass = klass
        self.endpoint = endpoint

    def _create_data_set_item(self, data):
        """create data_set
        """
        data_set_item = self._klass(**data)
        return data_set_item

    def _load_data_set(self):
        """Loads data """
        self.data_list = collect_data_from_api(self.endpoint)
        self._data_set = list(map(self._create_data_set_item, self.data_list))

    def __iter__(self):
        """iterator function"""

        self._load_data_set()
        for data in self._data_set:
            yield data

    def __len__(self):
        self._load_data_set()
        return len(self._data_set)




